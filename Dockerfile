#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["FilmsMicroService/FilmsMicroService.csproj", "./MicroService/FilmsMicroService.csproj"]
COPY ["FilmsMicroServiceTests/FilmsMicroServiceTests.csproj", "./Tests/FilmsMicroServiceTests.csproj"]
RUN dotnet restore "./MicroService/FilmsMicroService.csproj"
RUN dotnet restore "./Tests/FilmsMicroServiceTests.csproj"
COPY ./FilmsMicroService ./MicroService/
COPY ./FilmsMicroServiceTests ./Tests/
RUN dotnet build "MicroService/FilmsMicroService.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "MicroService/FilmsMicroService.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "FilmsMicroService.dll"]
