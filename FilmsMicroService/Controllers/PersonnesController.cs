﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FilmsMicroService.Models.EntityFramework;
using FilmsMicroService.Models.Repository;
using Microsoft.AspNetCore.Authorization;

namespace FilmsMicroService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PersonnesController : ControllerBase
    {
        private readonly IEntityRepository<Personne> dataRepository;

        public PersonnesController(IEntityRepository<Personne> dataRepo)
        {
            dataRepository = dataRepo;
        }

        // GET: api/Personnes
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Personne>>> GetPersonne()
        {
            return await dataRepository.GetAll();
        }

        // GET: api/Personnes/5
        [HttpGet("[action]/{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Personne>> ById(string id)
        {
            var personne = await dataRepository.GetById(id);

            if (personne == null)
            {
                return NotFound();
            }

            return personne;
        }

        [HttpGet("[action]/{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Personne>> ByNom(string nom)
        {
            var personne = await dataRepository.GetByString(nom);

            if (personne == null)
            {
                return NotFound();
            }

            return personne;
        }

        // PUT: api/Personnes/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> PutPersonne(string id, Personne personne)
        {
            if (id != personne.PersonneId)
            {
                return BadRequest("id incorrect");
            }

            var personneToUpdate = await dataRepository.GetById(id);
            if (personneToUpdate == null)
            {
                return NotFound("personne non trouvée");
            }
            dataRepository.Update(personneToUpdate.Value, personne);

            return NoContent();
        }

        // POST: api/Personnes
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Personne>> PostPersonne(Personne personne)
        {
            await dataRepository.Add(personne);
            return CreatedAtAction("ById", new { id = personne.PersonneId }, personne);
        }

        // DELETE: api/Personnes/5
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeletePersonne(string id)
        {
            var film = await dataRepository.GetById(id);
            if (film == null)
            {
                return NotFound("personne non trouvée");
            }

            await dataRepository.Delete(film.Value);

            return NoContent();
        }
    }
}
