﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FilmsMicroService.Models.EntityFramework;
using FilmsMicroService.Models.Repository;
using Microsoft.AspNetCore.Authorization;
using FilmsMicroService.Models.Dto;
using FilmsMicroService.Services;
using System.Text.Json.Nodes;
using System.Text.Json;

namespace FilmsMicroService.Controllers
{
    [Route("")]
    [ApiController]
    public class FilmsController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IEntityRepository<Film> dataRepository;

        public FilmsController(IEntityRepository<Film> dataRepo, IConfiguration config)
        {
            _config = config;
            dataRepository = dataRepo;
        }

        // GET: api/Films
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Film>>> GetFilms()
        {
            return await dataRepository.GetAll();
        }

        // GET: api/Films/5
        [HttpGet("[action]/{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Film>> ById(string id)
        {
            var film = await dataRepository.GetById(id);

            if (film == null)
            {
                return NotFound();
            }

            var filmReturn = film.Value;
            string parametres = "client_id=" + _config["BetaSeries:API_Key"] + "&imdb_id=" + filmReturn.FilmId;
            HttpContent content = Connect.Get(_config["BetaSeries:API_Url"] + "movies/movie?" + parametres);
            if (content != null)
            {
                JsonNode data = await JsonSerializer.DeserializeAsync<JsonNode>(await content.ReadAsStreamAsync());
                filmReturn.Synopsis = data["movie"]["synopsis"].ToString();
                filmReturn.ImageUrl = data["movie"]["poster"].ToString();
            }

            return filmReturn;
        }

        // GET: api/Films/5
        [HttpGet("[action]/{titre}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Film>> ByTitre(string titre)
        {
            var film = await dataRepository.GetByString(titre);

            if (film == null)
            {
                return NotFound();
            }

            var filmReturn = film.Value;
            string parametres = "client_id=" + _config["BetaSeries:API_Key"] + "&imdb_id=" + filmReturn.FilmId;
            HttpContent content = Connect.Get(_config["BetaSeries:API_Url"] + "movies/movie?" + parametres);
            if (content != null)
            {
                JsonNode data = await JsonSerializer.DeserializeAsync<JsonNode>(await content.ReadAsStreamAsync());
                filmReturn.Synopsis = data["movie"]["synopsis"].ToString();
                filmReturn.ImageUrl = data["movie"]["poster"].ToString();
                filmReturn.Note = (float)data["movie"]["notes"]["mean"];
            }

            return filmReturn;
        }

        // PUT: api/Films/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> PutFilm(string id, Film film)
        {
            if (id != film.FilmId)
            {
                return BadRequest("id incorrect");
            }

            var filmToUpdate = await dataRepository.GetById(id);
            if(filmToUpdate == null)
            {
                return NotFound("film non trouvé");
            }
            dataRepository.Update(filmToUpdate.Value, film);

            return NoContent();
        }

        // POST: api/Films
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Film>> PostFilm(Film film)
        {
            await dataRepository.Add(film);
            return CreatedAtAction("ById", new { id = film.FilmId }, film);
        }

        // GET: api/Films/5
        [HttpPost("[action]")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<Film>>> ByIds(ListFilmsId list)
        {
            List<Film> films = new List<Film>();
            foreach(var id in list.ids)
            {
                var film = await dataRepository.GetById(id);
                var filmToAdd = film.Value;
                if (filmToAdd != null)
                {
                    string parametres = "client_id=" + _config["BetaSeries:API_Key"] + "&imdb_id=" + filmToAdd.FilmId;
                    HttpContent content = Connect.Get(_config["BetaSeries:API_Url"] + "movies/movie?" + parametres);
                    if (content != null)
                    {
                        JsonNode data = await JsonSerializer.DeserializeAsync<JsonNode>(await content.ReadAsStreamAsync());
                        filmToAdd.Synopsis = data["movie"]["synopsis"].ToString();
                        filmToAdd.ImageUrl = data["movie"]["poster"].ToString();
                        filmToAdd.Note = (float)data["movie"]["notes"]["mean"];
                    }
                    films.Add(filmToAdd);
                }
            }
            return films;
        }

        // DELETE: api/Films/5
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeleteFilm(string id)
        {
            var film = await dataRepository.GetById(id);
            if (film == null)
            {
                return NotFound("film non trouvé");
            }

            await dataRepository.Delete(film.Value);

            return NoContent();
        }
    }
}
