﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FilmsMicroService.Migrations
{
    /// <inheritdoc />
    public partial class addlimitfilmid : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "flm_id",
                schema: "public",
                table: "t_e_film_flm",
                type: "character varying(10)",
                maxLength: 10,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "FilmsFilmId",
                schema: "public",
                table: "FilmGenre",
                type: "character varying(10)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "flm_id",
                schema: "public",
                table: "t_e_film_flm",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(10)",
                oldMaxLength: 10);

            migrationBuilder.AlterColumn<string>(
                name: "FilmsFilmId",
                schema: "public",
                table: "FilmGenre",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(10)");
        }
    }
}
