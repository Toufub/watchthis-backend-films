﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FilmsMicroService.Migrations
{
    /// <inheritdoc />
    public partial class relationconstraintcast : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_t_c_cast_cst_t_e_film_flm_FilmId",
                schema: "public",
                table: "t_c_cast_cst");

            migrationBuilder.DropForeignKey(
                name: "FK_t_c_cast_cst_t_p_personne_prs_PersonneId",
                schema: "public",
                table: "t_c_cast_cst");

            migrationBuilder.AddForeignKey(
                name: "fk_film_cast",
                schema: "public",
                table: "t_c_cast_cst",
                column: "FilmId",
                principalSchema: "public",
                principalTable: "t_e_film_flm",
                principalColumn: "flm_id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "pk_personne_cast",
                schema: "public",
                table: "t_c_cast_cst",
                column: "PersonneId",
                principalSchema: "public",
                principalTable: "t_p_personne_prs",
                principalColumn: "prs_id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_film_cast",
                schema: "public",
                table: "t_c_cast_cst");

            migrationBuilder.DropForeignKey(
                name: "pk_personne_cast",
                schema: "public",
                table: "t_c_cast_cst");

            migrationBuilder.AddForeignKey(
                name: "FK_t_c_cast_cst_t_e_film_flm_FilmId",
                schema: "public",
                table: "t_c_cast_cst",
                column: "FilmId",
                principalSchema: "public",
                principalTable: "t_e_film_flm",
                principalColumn: "flm_id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_t_c_cast_cst_t_p_personne_prs_PersonneId",
                schema: "public",
                table: "t_c_cast_cst",
                column: "PersonneId",
                principalSchema: "public",
                principalTable: "t_p_personne_prs",
                principalColumn: "prs_id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
