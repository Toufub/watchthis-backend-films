﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace FilmsMicroService.Migrations
{
    /// <inheritdoc />
    public partial class createfilmandgenretable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "t_e_film_flm",
                schema: "public",
                columns: table => new
                {
                    flmid = table.Column<string>(name: "flm_id", type: "text", nullable: false),
                    flmtype = table.Column<int>(name: "flm_type", type: "integer", nullable: false),
                    flmtitre = table.Column<string>(name: "flm_titre", type: "character varying(128)", maxLength: 128, nullable: false),
                    flmannee = table.Column<int>(name: "flm_annee", type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_e_film_flm", x => x.flmid);
                });

            migrationBuilder.CreateTable(
                name: "t_g_genre_gnr",
                schema: "public",
                columns: table => new
                {
                    gnrid = table.Column<int>(name: "gnr_id", type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    gnrnom = table.Column<string>(name: "gnr_nom", type: "character varying(25)", maxLength: 25, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_g_genre_gnr", x => x.gnrid);
                });

            migrationBuilder.CreateTable(
                name: "FilmGenre",
                schema: "public",
                columns: table => new
                {
                    FilmsFilmId = table.Column<string>(type: "text", nullable: false),
                    GenresGenreId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilmGenre", x => new { x.FilmsFilmId, x.GenresGenreId });
                    table.ForeignKey(
                        name: "FK_FilmGenre_t_e_film_flm_FilmsFilmId",
                        column: x => x.FilmsFilmId,
                        principalSchema: "public",
                        principalTable: "t_e_film_flm",
                        principalColumn: "flm_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FilmGenre_t_g_genre_gnr_GenresGenreId",
                        column: x => x.GenresGenreId,
                        principalSchema: "public",
                        principalTable: "t_g_genre_gnr",
                        principalColumn: "gnr_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FilmGenre_GenresGenreId",
                schema: "public",
                table: "FilmGenre",
                column: "GenresGenreId");

            migrationBuilder.CreateIndex(
                name: "IX_t_g_genre_gnr_gnr_nom",
                schema: "public",
                table: "t_g_genre_gnr",
                column: "gnr_nom",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FilmGenre",
                schema: "public");

            migrationBuilder.DropTable(
                name: "t_e_film_flm",
                schema: "public");

            migrationBuilder.DropTable(
                name: "t_g_genre_gnr",
                schema: "public");
        }
    }
}
