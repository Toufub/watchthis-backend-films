﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FilmsMicroService.Migrations
{
    /// <inheritdoc />
    public partial class foreignkeycast : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PersonneId",
                schema: "public",
                table: "t_c_cast_cst",
                newName: "prs_id");

            migrationBuilder.RenameColumn(
                name: "FilmId",
                schema: "public",
                table: "t_c_cast_cst",
                newName: "flm_id");

            migrationBuilder.RenameIndex(
                name: "IX_t_c_cast_cst_PersonneId",
                schema: "public",
                table: "t_c_cast_cst",
                newName: "IX_t_c_cast_cst_prs_id");

            migrationBuilder.RenameIndex(
                name: "IX_t_c_cast_cst_FilmId",
                schema: "public",
                table: "t_c_cast_cst",
                newName: "IX_t_c_cast_cst_flm_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "prs_id",
                schema: "public",
                table: "t_c_cast_cst",
                newName: "PersonneId");

            migrationBuilder.RenameColumn(
                name: "flm_id",
                schema: "public",
                table: "t_c_cast_cst",
                newName: "FilmId");

            migrationBuilder.RenameIndex(
                name: "IX_t_c_cast_cst_prs_id",
                schema: "public",
                table: "t_c_cast_cst",
                newName: "IX_t_c_cast_cst_PersonneId");

            migrationBuilder.RenameIndex(
                name: "IX_t_c_cast_cst_flm_id",
                schema: "public",
                table: "t_c_cast_cst",
                newName: "IX_t_c_cast_cst_FilmId");
        }
    }
}
