﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FilmsMicroService.Migrations
{
    /// <inheritdoc />
    public partial class removeprenompersonne : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_t_c_cast_cst_t_p_personne_prs_PersonneId",
                schema: "public",
                table: "t_c_cast_cst");

            migrationBuilder.DropColumn(
                name: "prs_prenom",
                schema: "public",
                table: "t_p_personne_prs");

            migrationBuilder.AlterColumn<string>(
                name: "prs_nom",
                schema: "public",
                table: "t_p_personne_prs",
                type: "character varying(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(64)",
                oldMaxLength: 64);

            migrationBuilder.AlterColumn<string>(
                name: "PersonneId",
                schema: "public",
                table: "t_c_cast_cst",
                type: "character varying(10)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(10)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_t_c_cast_cst_t_p_personne_prs_PersonneId",
                schema: "public",
                table: "t_c_cast_cst",
                column: "PersonneId",
                principalSchema: "public",
                principalTable: "t_p_personne_prs",
                principalColumn: "prs_id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_t_c_cast_cst_t_p_personne_prs_PersonneId",
                schema: "public",
                table: "t_c_cast_cst");

            migrationBuilder.AlterColumn<string>(
                name: "prs_nom",
                schema: "public",
                table: "t_p_personne_prs",
                type: "character varying(64)",
                maxLength: 64,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(128)",
                oldMaxLength: 128);

            migrationBuilder.AddColumn<string>(
                name: "prs_prenom",
                schema: "public",
                table: "t_p_personne_prs",
                type: "character varying(64)",
                maxLength: 64,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "PersonneId",
                schema: "public",
                table: "t_c_cast_cst",
                type: "character varying(10)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(10)");

            migrationBuilder.AddForeignKey(
                name: "FK_t_c_cast_cst_t_p_personne_prs_PersonneId",
                schema: "public",
                table: "t_c_cast_cst",
                column: "PersonneId",
                principalSchema: "public",
                principalTable: "t_p_personne_prs",
                principalColumn: "prs_id");
        }
    }
}
