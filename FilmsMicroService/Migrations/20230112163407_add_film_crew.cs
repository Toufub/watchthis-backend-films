﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace FilmsMicroService.Migrations
{
    /// <inheritdoc />
    public partial class addfilmcrew : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "t_p_personne_prs",
                schema: "public",
                columns: table => new
                {
                    prsid = table.Column<string>(name: "prs_id", type: "character varying(10)", maxLength: 10, nullable: false),
                    prsnom = table.Column<string>(name: "prs_nom", type: "character varying(64)", maxLength: 64, nullable: false),
                    prsprenom = table.Column<string>(name: "prs_prenom", type: "character varying(64)", maxLength: 64, nullable: false),
                    prsbirth = table.Column<int>(name: "prs_birth", type: "integer", nullable: false),
                    prsdeath = table.Column<int>(name: "prs_death", type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_p_personne_prs", x => x.prsid);
                });

            migrationBuilder.CreateTable(
                name: "t_c_cast_cst",
                schema: "public",
                columns: table => new
                {
                    cstid = table.Column<int>(name: "cst_id", type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PersonneId = table.Column<string>(type: "character varying(10)", nullable: true),
                    FilmId = table.Column<string>(type: "character varying(10)", nullable: false),
                    cstrole = table.Column<string>(name: "cst_role", type: "character varying(32)", maxLength: 32, nullable: false),
                    cstordre = table.Column<int>(name: "cst_ordre", type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_c_cast_cst", x => x.cstid);
                    table.ForeignKey(
                        name: "FK_t_c_cast_cst_t_e_film_flm_FilmId",
                        column: x => x.FilmId,
                        principalSchema: "public",
                        principalTable: "t_e_film_flm",
                        principalColumn: "flm_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_t_c_cast_cst_t_p_personne_prs_PersonneId",
                        column: x => x.PersonneId,
                        principalSchema: "public",
                        principalTable: "t_p_personne_prs",
                        principalColumn: "prs_id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_t_c_cast_cst_FilmId",
                schema: "public",
                table: "t_c_cast_cst",
                column: "FilmId");

            migrationBuilder.CreateIndex(
                name: "IX_t_c_cast_cst_PersonneId",
                schema: "public",
                table: "t_c_cast_cst",
                column: "PersonneId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_c_cast_cst",
                schema: "public");

            migrationBuilder.DropTable(
                name: "t_p_personne_prs",
                schema: "public");
        }
    }
}
