﻿namespace FilmsMicroService.Models.Dto
{
    public class ListFilmsId
    {
        public List<string> ids { get; set; }
    }
}
