﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FilmsMicroService.Models.EntityFramework
{
    [Table("t_e_film_flm")]
    public partial class Film
    {
        public Film()
        {
            Genres = new HashSet<Genre>();
            Casts = new HashSet<Cast>();
        }

        [Key]
        [Column("flm_id")]
        [StringLength(10)]
        public string FilmId { get; set; }

        [Required]
        [Column("flm_type")]
        public Type Type { get; set; }

        [Required]
        [Column("flm_titre")]
        [StringLength(128)]
        public string Titre { get; set; }

        [Required]
        [Column("flm_annee")]
        public int Annee { get; set; }

        [NotMapped]
        public string ImageUrl { get; set;}

        [NotMapped]
        public string Synopsis { get; set; }

        [NotMapped]
        public float Note { get; set; }

        public virtual ICollection<Cast> Casts { get; set; }
        public virtual ICollection<Genre> Genres { get; set; }
    }

    public enum Type
    {
        _movie,
        _short,
        _tvMovie,
        _tvShort,
    }
}
