﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FilmsMicroService.Models.EntityFramework
{

    [Table("t_p_personne_prs")]
    public partial class Personne
    {
        public Personne()
        {
            Casts = new HashSet<Cast>();
        }

        [Key]
        [Column("prs_id")]
        [StringLength(10)]
        public string PersonneId { get; set; }

        [Column("prs_nom")]
        [StringLength(128)]
        public string? Nom { get; set; }

        [Column("prs_birth")]
        public int Birth { get; set; }

        [Column("prs_death")]
        public int Death { get; set; }

        [JsonIgnore]
        public virtual ICollection<Cast> Casts { get; set; }
    }

}
