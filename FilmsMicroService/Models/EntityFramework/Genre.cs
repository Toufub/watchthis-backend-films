﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using System.Text.Json.Serialization;

namespace FilmsMicroService.Models.EntityFramework
{
    [Table("t_g_genre_gnr")]
    [Index(nameof(Nom), IsUnique = true)]
    public class Genre
    {
        public Genre()
        {
            Films = new HashSet<Film>();
        }

        [Key]
        [Column("gnr_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GenreId { get; set; }

        [Required]
        [Column("gnr_nom")]
        [StringLength(25)]
        public string Nom { get; set; }

        [JsonIgnore]
        public virtual ICollection<Film> Films { get; set; }

    }
}
