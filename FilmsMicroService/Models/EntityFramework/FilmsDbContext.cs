﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using static FilmsMicroService.Models.EntityFramework.Film;
using FilmsMicroService.Models.EntityFramework;
using Microsoft.CodeAnalysis;

namespace FilmsMicroService.Models.EntityFramework;

public partial class FilmsDbContext : DbContext

{
    public static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => builder.AddConsole());

    public FilmsDbContext()
    {
    }

    public FilmsDbContext(DbContextOptions<FilmsDbContext> options)
        : base(options)
    {
    }
    public virtual DbSet<Film> Films { get; set; } = null!;
    public virtual DbSet<Genre> Genres { get; set; } = null!;

    public virtual DbSet<Cast> Casts { get; set; } = null!;
    public virtual DbSet<Personne> Personnes { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseLoggerFactory(MyLoggerFactory).EnableSensitiveDataLogging();
        }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema("public");

        modelBuilder.Entity<Cast>(entity =>
        {
            entity.HasOne(c => c.Film)
                .WithMany(f => f.Casts)
                .HasForeignKey(c => c.FilmId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("fk_film_cast");

            entity.HasOne(c => c.Personne)
                .WithMany(p => p.Casts)
                .HasForeignKey(c => c.PersonneId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("pk_personne_cast");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
