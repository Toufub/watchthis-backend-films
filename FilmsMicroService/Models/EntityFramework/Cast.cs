﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace FilmsMicroService.Models.EntityFramework
{
    [Table("t_c_cast_cst")]
    public class Cast
    {
        public Cast()
        {

        }

        [Key]
        [Column("cst_id")]
        [JsonIgnore]
        public int CastId { get; set; }

        [Column("flm_id")]
        public string FilmId { get; set; }

        [Column("prs_id")]
        [JsonIgnore]
        public string PersonneId { get; set; }

        [ForeignKey("PersonneId")]
        [InverseProperty("Casts")]
        public virtual Personne Personne { get; set; }

        [ForeignKey("FilmId")]
        [InverseProperty("Casts")]
        [JsonIgnore]
        public virtual Film Film { get; set; }

        [Required]
        [Column("cst_role")]
        [StringLength(32)]
        public string Role { get; set; }

        [Required]
        [Column("cst_ordre")]
        public int Ordre { get; set; }
    }
}
