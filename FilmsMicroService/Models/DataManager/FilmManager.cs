﻿using FilmsMicroService.Models.EntityFramework;
using FilmsMicroService.Models.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace FilmsMicroService.Models.DataManager
{
    public class FilmManager : IEntityRepository<Film>
    {
        private readonly FilmsDbContext filmsDbContext;
        public FilmManager() { }
        public FilmManager(FilmsDbContext context)
        {
            filmsDbContext = context;
        }
        public async Task<ActionResult<IEnumerable<Film>>> GetAll()
        {
            return await filmsDbContext.Films
                .Include(f => f.Genres)
                .ToListAsync();
        }
        public async Task<ActionResult<Film>> GetById(string id)
        {
            return await filmsDbContext.Films
                .Include(f => f.Casts)
                .ThenInclude(c => c.Personne)
                .FirstOrDefaultAsync(f => f.FilmId == id);
        }
        public async Task<ActionResult<Film>> GetByString(string titre)
        {
            return await filmsDbContext.Films
                .Include(f => f.Casts)
                .ThenInclude(c => c.Personne)
                .FirstOrDefaultAsync(f => f.Titre.ToUpper() == titre.ToUpper());
        }
        public async Task Add(Film entity)
        {
            await filmsDbContext.Films.AddAsync(entity);
            await filmsDbContext.SaveChangesAsync();
        }
        public async Task Update(Film film, Film entity)
        {
            filmsDbContext.Entry(film).State = EntityState.Modified;
            film.Titre = entity.Titre;
            film.Annee = entity.Annee;
            film.Genres = entity.Genres;
            film.Type = entity.Type;
            film.Casts = entity.Casts;
            await filmsDbContext.SaveChangesAsync();
        }

        public async Task Delete(Film film)
        {
            filmsDbContext.Films.Remove(film);
            await filmsDbContext.SaveChangesAsync();
        }
    }
}
