﻿using FilmsMicroService.Models.EntityFramework;
using FilmsMicroService.Models.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace FilmsMicroService.Models.DataManager
{
    public class PersonneManager : IEntityRepository<Personne>
    {
        private readonly FilmsDbContext filmsDbContext;
        public PersonneManager() { }
        public PersonneManager(FilmsDbContext context)
        {
            filmsDbContext = context;
        }

        public async Task<ActionResult<IEnumerable<Personne>>> GetAll()
        {
            return await filmsDbContext.Personnes.ToListAsync();
        }

        public async Task<ActionResult<Personne>> GetById(string id)
        {
            return await filmsDbContext.Personnes
                .Include(p => p.Casts)
                .ThenInclude(c => c.Film)
                .FirstOrDefaultAsync(p => p.PersonneId == id) ;
        }

        public async Task<ActionResult<Personne>> GetByString(string nom)
        {
            return await filmsDbContext.Personnes
                .Include(p => p.Casts)
                .ThenInclude(c => c.Film)
                .FirstOrDefaultAsync(p => p.Nom.ToUpper() == nom.ToUpper());
        }

        public async Task Add(Personne entity)
        {
            await filmsDbContext.Personnes.AddAsync(entity);
            await filmsDbContext.SaveChangesAsync();
        }

        public async Task Update(Personne entityToUpdate, Personne entity)
        {
            filmsDbContext.Entry(entityToUpdate).State = EntityState.Modified;
            entityToUpdate.Nom = entity.Nom;
            entityToUpdate.Birth = entity.Birth;
            entityToUpdate.Death = entity.Death;
            entityToUpdate.Casts = entity.Casts;
            await filmsDbContext.SaveChangesAsync();
        }

        public async Task Delete(Personne entity)
        {
            filmsDbContext.Personnes.Remove(entity);
            await filmsDbContext.SaveChangesAsync();
        }
    }
}
