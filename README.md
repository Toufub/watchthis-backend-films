# WatchThis - Backend Films

## Fonctionnement API Film
### FindAll - Get

Necessite un JWT Token
Code de retour :
* 200 : OK
* 401 : Unauthorized

`/api/films/`

Structure d'un film : 
````
    {
        "filmId": "tt0000001",
        "type": 1,
        "titre": "Carmencita",
        "annee": 1894,
        "casts": [],
        "genres": [],
	  "imageUrl" : https://......,
	  "synospsis" : ........
    }
````

### ById - GET

Necessite un JWT token
Code de retour :
* 200 : OK
* 404 : Not found
* 401 : Unauthorized

`/api/films/byid/{id}`

Renvoie un film avec le casting :
Structure film (plus haut) et structure de casts : 
````

{
            "filmId": "tt0000001",
            "personne": {
                "personneId": "nm1588970",
                "nom": "Carmencita",
                "birth": 1868,
                "death": 1910
            },
            "role": "self",
            "ordre": 1
}
````

### ByTitre - GET

Necessite un JWT token
Code de retour :
* 200 : OK
* 404 : Not found
* 401 : Unauthorized

`/api/films/bytitre/{titre}`

Renvoie un film avec le casting.

### ByIds - POST

Necessite un JWT Token
Code de retour :
* 200 : OK
* 401 : Unauthorized

`/api/films/byids`

Structure de données : 
````
{
    "ids": ["tt0000001", "tt0000004", "tt0000002", ............]
}
````
